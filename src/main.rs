use futures::TryStreamExt;

use log::{debug, error};

use hyper::{
    header,
    service::{make_service_fn, service_fn},
    {Body, Method, Request, Response, Server},
};

use common::{APPLICATION_ID, FROM_NUMBER};

/// An empty 200 HTTP response, put into
/// a function for easy reuse
fn empty_200() -> Response<Body> {
    Response::builder()
        .status(200)
        .body(Body::from(String::new()))
        .unwrap()
}

#[tokio::main]
async fn main() -> Result<(), hyper::Error> {
    env_logger::init();

    let addr = ([0, 0, 0, 0], 80).into();

    let make_svc = make_service_fn(|_| async { Ok::<_, reqwest::Error>(service_fn(do_work)) });

    let server = Server::bind(&addr).serve(make_svc);

    if let Err(e) = server.await {
        error!("server error: {}", e);
    }

    Ok(())
}

async fn do_work(req: Request<Body>) -> Result<Response<Body>, hyper::Error> {
    let (parts, body) = req.into_parts();
    match (parts.method, parts.uri.path()) {
        (Method::POST, "/api/webhooks") => {
            let entire_body = body
                .try_fold(Vec::new(), |mut data, chunk| async move {
                    data.extend_from_slice(&chunk);
                    Ok(data)
                })
                .await?;

            let data: serde_json::Value = match serde_json::from_slice(&entire_body) {
                Ok(v) => v,
                Err(..) => return Ok(empty_200()),
            };

            debug!("{:?}", data);

            // The general structure of the request is
            /*
            [
                {
                    ...
                    "message": {
                        ...
                        "text": "XXX",
                        "from": "XXX",
                        ...
                    },
                    ...
                },
            ]
            */
            let (code, number) = match (|| {
                let message = data
                    .as_array()?
                    .get(0)?
                    .as_object()?
                    .get("message")?
                    .as_object()?;

                let code = message.get("text")?.as_str()?.trim().to_ascii_lowercase();
                let number = message.get("from")?.as_str()?.trim();

                Some((code, number))
            })() {
                Some(v) => v,
                None => return Ok(empty_200()),
            };

            let message = match get_text_message(code).await {
                Ok(v) => v,
                Err(..) => return Ok(empty_200()),
            };

            send_text(number, message).await;
        }
        _ => {}
    }
    Ok(empty_200())
}

/// Makes request to coconut backend, returning the text content
/// given based on the code provided.
#[allow(dead_code, unused_variables)]
async fn get_text_message(code: String) -> Result<String, Box<dyn std::error::Error>> {
    todo!()
}

/// <https://dev.bandwidth.com/messaging/methods/messages/createMessage.html>
async fn send_text(number: &str, message: String) {
    let client = reqwest::Client::new();
    client
        .post(&format!(
            "https://messaging.bandwidth.com/api/v2/users/{APPLICATION_ID}/messages",
            APPLICATION_ID = APPLICATION_ID
        ))
        .header(header::CONTENT_TYPE, "application/json; charset=utf-8")
        .header(header::AUTHORIZATION, "")
        .body(
            serde_json::json!({
                "to": [number],
                "from": FROM_NUMBER,
                "text": message,
                "applicationId": APPLICATION_ID,
                "tag": "",
            })
            .to_string(),
        );
}
